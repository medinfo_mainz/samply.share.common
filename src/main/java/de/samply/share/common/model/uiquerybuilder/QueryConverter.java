/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.common.model.uiquerybuilder;

import de.samply.share.model.osse.Attribute;
import de.samply.share.model.osse.MultivalueAttribute;
import de.samply.share.model.osse.And;
import de.samply.share.model.osse.Between;
import de.samply.share.model.osse.ConditionType;
import de.samply.share.model.osse.Eq;
import de.samply.share.model.osse.Geq;
import de.samply.share.model.osse.Gt;
import de.samply.share.model.osse.In;
import de.samply.share.model.osse.IsNotNull;
import de.samply.share.model.osse.IsNull;
import de.samply.share.model.osse.Leq;
import de.samply.share.model.osse.Like;
import de.samply.share.model.osse.Lt;
import de.samply.share.model.osse.Neq;
import de.samply.share.model.osse.Or;
import de.samply.share.model.osse.Query;
import de.samply.share.model.osse.View;
import de.samply.share.model.osse.Where;
import de.samply.share.model.osse.RangeAttribute;
import de.samply.share.model.osse.ObjectFactory;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;
import org.omnifaces.model.tree.ListTreeModel;
import org.omnifaces.model.tree.TreeModel;
import org.xml.sax.InputSource;



import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Offer Methods to serialize and deserialize a Query
 */
public class QueryConverter {

    /** The Constant logger. */
    private static final Logger logger = Logger.getLogger(QueryConverter.class);

    /** The Constant CONJUNCTION_GROUP. */
    private static final String CONJUNCTION_GROUP = "conjunctionGroup";

    /**
     * Prohibit class instantiation since it only offers static methods
     */
    private QueryConverter() {
    }

    /**
     * Transforms a CCP Query to an OSSE Query
     *
     * Is used to transform queries that are transmitted via REST interface from central search
     *
     * @param ccpQuery
     *            the query in the ccp namespace
     * @return XML the query in the osse namespace
     * @throws JAXBException
     */
    public static de.samply.share.model.osse.Query convertCcpQueryToOsseQuery(de.samply.share.model.ccp.Query ccpQuery) throws JAXBException {
        ccpQuery = insertRootGroup(ccpQuery);
        String ccpQueryString = ccpQueryToXml(ccpQuery);
        ccpQueryString = ccpQueryString.replace("/ccp/", "/osse/");
        de.samply.share.model.osse.Query osseQuery = xmlToQuery(ccpQueryString);
        return osseQuery;
    }

    private static de.samply.share.model.ccp.Query insertRootGroup(de.samply.share.model.ccp.Query oldQuery) {
        Object firstChild = oldQuery.getWhere().getAndOrEqOrLike().get(0);
        if (firstChild != null && (firstChild.getClass() == de.samply.share.model.ccp.And.class || firstChild.getClass() == de.samply.share.model.ccp.Or.class) ) {
            return oldQuery;
        } else {
            de.samply.share.model.ccp.Query newQuery = new de.samply.share.model.ccp.Query();
            de.samply.share.model.ccp.And and = new de.samply.share.model.ccp.And();
            de.samply.share.model.ccp.Where where = new de.samply.share.model.ccp.Where();

            and.getAndOrEqOrLike().addAll(oldQuery.getWhere().getAndOrEqOrLike());
            where.getAndOrEqOrLike().add(and);

            newQuery.setGroupBy(oldQuery.getGroupBy());
            newQuery.setId(oldQuery.getId());
            newQuery.setOrderBy(oldQuery.getOrderBy());
            newQuery.setWhere(where);
            return newQuery;
        }
    }

    /**
     * Serializes a CCP query to XML
     *
     * @param view
     *            the {@link View} associated with a query
     * @return XML representation of a query
     * @throws JAXBException
     */
    public static String ccpQueryToXml(de.samply.share.model.ccp.Query query) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(de.samply.share.model.ccp.Query.class, de.samply.share.model.ccp.ObjectFactory.class);
        return asString(jaxbContext, query);
    }

    /**
     * De-serialises a CCP query from XML
     *
     * @param xml
     *            representation of a query - {@link View}
     * @return {@link View} with a query object
     * @throws JAXBException
     */
    public static de.samply.share.model.ccp.Query ccpXmlToQuery(String xml) throws JAXBException {
        InputSource inputSource = new InputSource(new StringReader(xml));

        JAXBContext jaxbContext = JAXBContext.newInstance(de.samply.share.model.ccp.Query.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        return (de.samply.share.model.ccp.Query) jaxbUnmarshaller.unmarshal(inputSource);
    }

    /**
     * Serializes a query to XML
     *
     * @param view
     *            the {@link View} associated with a query
     * @return XML representation of a query
     * @throws JAXBException
     */
    public static String queryToXml(Query query) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Query.class, ObjectFactory.class);
        return asString(jaxbContext, query);
    }

    /**
     * De-serialises a query from XML
     *
     * @param xml
     *            representation of a query - {@link View}
     * @return {@link View} with a query object
     * @throws JAXBException
     */
    public static Query xmlToQuery(String xml) throws JAXBException {
        InputSource inputSource = new InputSource(new StringReader(xml));

        JAXBContext jaxbContext = JAXBContext.newInstance(Query.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        return (Query) jaxbUnmarshaller.unmarshal(inputSource);
    }

    public static String asString(JAXBContext jaxbContext, Object jaxbElement) throws JAXBException {

        java.io.StringWriter stringWriter = new StringWriter();

        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
        marshaller.marshal(jaxbElement, stringWriter);

        return stringWriter.toString();
    }

    public static TreeModel<QueryItem> queryStringToTree(String queryString) {
        Query query = new Query();
        Where where = new Where();
        query.setWhere(where);

        if (queryString == null || queryString.length() < 1) {
            logger.error("Error...empty query String was submitted...");
            return queryToTree(query);
        }
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Query.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            StringReader stringReader = new StringReader(queryString);
            query = (Query) unmarshaller.unmarshal(stringReader);
        } catch (JAXBException e) {
            logger.debug("JAXB Error while converting querystring to tree. Returning empty query");
        }
        return queryToTree(query);
    }

    public static TreeModel<QueryItem> queryToTree(Query query) {
        TreeModel<QueryItem> criteriaTree = new ListTreeModel<>();

        Where where = query.getWhere();
        if (where != null) {
            for (int i = 0; i < where.getAndOrEqOrLike().size(); ++i) {
                criteriaTree = visitNode(criteriaTree, where.getAndOrEqOrLike().get(i));
            }
        }
        return criteriaTree;
    }

    public static Query treeToQuery(TreeModel<QueryItem> queryTree) {
        Query query = new Query();
        Where where = new Where();

        where = (Where) visitNodeAndAppendToQuery(queryTree, where);
        query.setWhere(where);
        try {
            logger.debug(queryToXml(query));
        } catch (JAXBException e) {
            throw new RuntimeException("Error printing query", e);
        }
        return query;
    }

    private static Object visitNodeAndAppendToQuery(TreeModel<QueryItem> node, ConditionType target) {
        if (node.isRoot()) {
            logger.debug("Visiting root");
            Where where = (Where) target;
            for (TreeModel<QueryItem> queryItem : node.getChildren()) {
                where = (Where) visitNodeAndAppendToQuery(queryItem, where);
            }
            return target;
        } else {
            logger.debug("visiting " + node.getIndex() + " - target: " + target.getClass());

            if (node.isLeaf()) {

                if (target.getClass() == Where.class) {
                    Where where = new Where();
                    for (TreeModel<QueryItem> qi : node.getChildren()) {
                        where = (Where) visitNodeAndAppendToQuery(qi, where);
                    }
                    return target;
                }

                ObjectFactory objectFactory = new ObjectFactory();
                QueryItem queryItem = node.getData();
                String operatorString = "";
                if (queryItem.getOperator() == null) {
                    logger.debug("operator is null");
                } else {
                    operatorString = queryItem.getOperator().toString();
                }
                Attribute attribute = new Attribute();
                attribute.setMdrKey(queryItem.getMdrId());
                JAXBElement<String> jaxbElement = objectFactory.createValue(convertDateString(queryItem.getValue()));
                attribute.setValue(jaxbElement);

                if (operatorString.equalsIgnoreCase(EnumOperator.EQUAL.toString())) {
                    Eq eq = new Eq();
                    eq.setAttribute(attribute);
                    target.getAndOrEqOrLike().add(eq);
                } else if (operatorString.equalsIgnoreCase(EnumOperator.GREATER.toString())) {
                    Gt gt = new Gt();
                    gt.setAttribute(attribute);
                    target.getAndOrEqOrLike().add(gt);
                } else if (operatorString.equalsIgnoreCase(EnumOperator.GREATER_OR_EQUAL.toString())) {
                    Geq geq = new Geq();
                    geq.setAttribute(attribute);
                    target.getAndOrEqOrLike().add(geq);
                } else if (operatorString.equalsIgnoreCase(EnumOperator.LESS_THEN.toString())) {
                    Lt lt = new Lt();
                    lt.setAttribute(attribute);
                    target.getAndOrEqOrLike().add(lt);
                } else if (operatorString.equalsIgnoreCase(EnumOperator.LESS_OR_EQUAL_THEN.toString())) {
                    Leq leq = new Leq();
                    leq.setAttribute(attribute);
                    target.getAndOrEqOrLike().add(leq);
                } else if (operatorString.equalsIgnoreCase(EnumOperator.LIKE.toString())) {
                    Like like = new Like();
                    // Check if the value is enclosed in wildcard characters. If not, add them.
                    // Currently, queries coming from central search do NOT contain wildcards. If they change that, this should handle it.
                    String oldValue = attribute.getValue().getValue();
                    StringBuilder newValue = new StringBuilder();
                    if (!oldValue.startsWith("%")) {
                        newValue.append("%");
                    }
                    newValue.append(oldValue);
                    if (!oldValue.endsWith("%")) {
                        newValue.append("%");
                    }
                    attribute.setValue(objectFactory.createValue(newValue.toString()));
                    like.setAttribute(attribute);
                    target.getAndOrEqOrLike().add(like);
                } else if (operatorString.equalsIgnoreCase(EnumOperator.NOT_EQUAL_TO.toString())) {
                    Neq neq = new Neq();
                    neq.setAttribute(attribute);
                    target.getAndOrEqOrLike().add(neq);
                } else if (operatorString.equalsIgnoreCase(EnumOperator.IS_NOT_NULL.toString())) {
                    IsNotNull inn = new IsNotNull();
                    inn.setMdrKey(attribute.getMdrKey());
                    target.getAndOrEqOrLike().add(inn);
                } else if (operatorString.equalsIgnoreCase(EnumOperator.IS_NULL.toString())) {
                    IsNull in = new IsNull();
                    in.setMdrKey(attribute.getMdrKey());
                    target.getAndOrEqOrLike().add(in);
                } else if (operatorString.equalsIgnoreCase(EnumOperator.BETWEEN.toString())) {
                    Between between = new Between();
                    RangeAttribute rangeAttr = new RangeAttribute();
                    rangeAttr.setMdrKey(queryItem.getMdrId());
                    rangeAttr.setLowerBound(convertDateString(queryItem.getLowerBound()));
                    rangeAttr.setUpperBound(convertDateString(queryItem.getUpperBound()));

                    between.setRangeAttribute(rangeAttr);

                    target.getAndOrEqOrLike().add(between);
                } else if (operatorString.equalsIgnoreCase(EnumOperator.IN.toString())) {
                    In in = new In();
                    MultivalueAttribute mvAttr = new MultivalueAttribute();
                    mvAttr.setMdrKey(queryItem.getMdrId());

                    for (String value : queryItem.getValues()) {
                        jaxbElement = objectFactory.createValue(value);
                        mvAttr.getValue().add(jaxbElement);
                    }

                    in.setMultivalueAttribute(mvAttr);
                    target.getAndOrEqOrLike().add(in);
                }

                return target;
            } else if (node.getData().getMdrId().equalsIgnoreCase(CONJUNCTION_GROUP)) {
                QueryItem queryItem = node.getData();

                if (queryItem.getConjunction().equals(EnumConjunction.OR)) {
                    Or or = new Or();
                    for (TreeModel<QueryItem> qi : node.getChildren()) {
                        or = (Or) visitNodeAndAppendToQuery(qi, or);
                    }
                    target.getAndOrEqOrLike().add(or);
                } else {
                    // Use AND as default as well
                    And and = new And();
                    for (TreeModel<QueryItem> qi : node.getChildren()) {
                        and = (And) visitNodeAndAppendToQuery(qi, and);
                    }
                    target.getAndOrEqOrLike().add(and);
                }

                return target;
            }
        }
        logger.fatal("This should never be reached");
        return null;
    }

    private static TreeModel<QueryItem> visitNode(TreeModel<QueryItem> parentNode, Object obj) {

        QueryItem qi = new QueryItem();
        TreeModel<QueryItem> newNode = new ListTreeModel<>();

        if (obj.getClass() == de.samply.share.model.osse.And.class) {
            qi.setConjunction(EnumConjunction.AND);
            qi.setMdrId("conjunctionGroup");
            qi.setRoot(parentNode.isRoot());
            newNode = parentNode.addChild(qi);
            for (int j = 0; j < ((And) obj).getAndOrEqOrLike().size(); ++j) {
                newNode = visitNode(newNode, ((And) obj).getAndOrEqOrLike().get(j));
            }
        } else if (obj.getClass() == de.samply.share.model.osse.Or.class) {
            qi.setConjunction(EnumConjunction.OR);
            qi.setMdrId("conjunctionGroup");
            qi.setRoot(parentNode.isRoot());
            newNode = parentNode.addChild(qi);
            for (int k = 0; k < ((Or) obj).getAndOrEqOrLike().size(); ++k) {
                newNode = visitNode(newNode, ((Or) obj).getAndOrEqOrLike().get(k));
            }
        } else if (obj.getClass() == de.samply.share.model.osse.Eq.class) {
            Eq eq = (Eq) obj;
            qi.setMdrId(eq.getAttribute().getMdrKey());
            qi.setValue(eq.getAttribute().getValue().getValue());
            qi.setOperator(EnumOperator.EQUAL);
            parentNode.addChild(qi);

        } else if (obj.getClass() == de.samply.share.model.osse.Geq.class) {
            Geq geq = (Geq) obj;
            qi.setMdrId(geq.getAttribute().getMdrKey());
            qi.setValue(geq.getAttribute().getValue().getValue());
            qi.setOperator(EnumOperator.GREATER_OR_EQUAL);
            parentNode.addChild(qi);

        } else if (obj.getClass() == de.samply.share.model.osse.Leq.class) {
            Leq leq = (Leq) obj;
            qi.setMdrId(leq.getAttribute().getMdrKey());
            qi.setValue(leq.getAttribute().getValue().getValue());
            qi.setOperator(EnumOperator.LESS_OR_EQUAL_THEN);
            parentNode.addChild(qi);

        } else if (obj.getClass() == de.samply.share.model.osse.Gt.class) {
            Gt gt = (Gt) obj;
            qi.setMdrId(gt.getAttribute().getMdrKey());
            qi.setValue(gt.getAttribute().getValue().getValue());
            qi.setOperator(EnumOperator.GREATER);
            parentNode.addChild(qi);

        } else if (obj.getClass() == de.samply.share.model.osse.Lt.class) {
            Lt lt = (Lt) obj;
            qi.setMdrId(lt.getAttribute().getMdrKey());
            qi.setValue(lt.getAttribute().getValue().getValue());
            qi.setOperator(EnumOperator.LESS_THEN);
            parentNode.addChild(qi);

        } else if (obj.getClass() == de.samply.share.model.osse.Like.class) {
            Like like = (Like) obj;
            qi.setMdrId(like.getAttribute().getMdrKey());
            String val = like.getAttribute().getValue().getValue();
            if (val != null && val.length() > 2 && val.startsWith("%") && val.endsWith("%")) {
                val = val.substring(1, val.length() - 1);
            }
            qi.setValue(val);
            qi.setOperator(EnumOperator.LIKE);
            parentNode.addChild(qi);

        } else if (obj.getClass() == de.samply.share.model.osse.Neq.class) {
            Neq neq = (Neq) obj;
            qi.setMdrId(neq.getAttribute().getMdrKey());
            qi.setValue(neq.getAttribute().getValue().getValue());
            qi.setOperator(EnumOperator.NOT_EQUAL_TO);
            parentNode.addChild(qi);

        } else if (obj.getClass() == de.samply.share.model.osse.IsNull.class) {
            IsNull isNull = (IsNull) obj;
            qi.setMdrId(isNull.getMdrKey());
            qi.setOperator(EnumOperator.IS_NULL);
            parentNode.addChild(qi);

        } else if (obj.getClass() == de.samply.share.model.osse.IsNotNull.class) {
            IsNotNull isNotNull = (IsNotNull) obj;
            qi.setMdrId(isNotNull.getMdrKey());
            qi.setOperator(EnumOperator.IS_NOT_NULL);
            parentNode.addChild(qi);

        } else if (obj.getClass() == de.samply.share.model.osse.Between.class) {
            Between between = (Between) obj;
            qi.setMdrId(between.getRangeAttribute().getMdrKey());
            qi.setLowerBound(between.getRangeAttribute().getLowerBound());
            qi.setUpperBound(between.getRangeAttribute().getUpperBound());
            qi.setOperator(EnumOperator.BETWEEN);
            parentNode.addChild(qi);

        } else if (obj.getClass() == de.samply.share.model.osse.In.class) {
            In in = (In) obj;
            qi.setMdrId(in.getMultivalueAttribute().getMdrKey());
            qi.setIn(in.getMultivalueAttribute());
            qi.setOperator(EnumOperator.IN);
            parentNode.addChild(qi);

        }
        return parentNode;
    }

        /**
     * Convert date string.
     *
     * @param d1 the d1
     * @return the string
     */
    public static String convertDateString(String d1) {
        if (d1 == null) {
            return null;
        }
        DateFormat sdff_in = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
        // DateFormat sdff_out = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss z", Locale.ENGLISH);
        DateFormat sdff_out = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);

        Date tmp;
        try {
            tmp = sdff_in.parse(d1);
            return sdff_out.format(tmp);
        } catch (ParseException e) {
            logger.debug("Can't parse or don't need to parse: " + d1 + " - returning as is");
        }
        return d1;

        // return sdff.format(sdff.parse(d1));
    }

    /**
     * Serializes a query to XML
     *
     * @param view
     *            the {@link View} associated with a query
     * @return XML representation of a query
     * @throws JAXBException
     */
    public static String viewToXml(View view) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(View.class);
        return asString(jaxbContext, view);
    }

    /**
     * De-serialises a query from XML
     *
     * @param xml
     *            representation of a query - {@link View}
     * @return {@link View} with a query object
     * @throws JAXBException
     */
    public static View xmlToView(String xml) throws JAXBException {
        InputSource inputSource = new InputSource(new StringReader(xml));

        JAXBContext jaxbContext = JAXBContext.newInstance(View.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        return (View) jaxbUnmarshaller.unmarshal(inputSource);
    }

}
