package de.samply.share.common.utils.oauth2;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import de.samply.auth.client.jwt.JWTAccessToken;

import de.samply.auth.client.jwt.JWTException;
import de.samply.auth.client.jwt.KeyLoader;
import de.samply.auth.rest.AccessTokenDTO;
import de.samply.auth.rest.AccessTokenRequestDTO;
import de.samply.auth.rest.KeyIdentificationDTO;
import de.samply.auth.rest.SignRequestDTO;
import de.samply.share.common.utils.ProjectInfo;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

// TODO: Auto-generated Javadoc
/**
 * The Class OAuthUtils.
 */
public class OAuthUtils {

    /** The Constant logger. */
    private static final Logger logger = Logger.getLogger(OAuthUtils.class);

    /**
     * Gets the access token.
     *
     * @param client the client
     * @param authHost the hostname of the oath server
     * @param keyId the id of the key in auth
     * @param privateKey the private key for logging in in auth
     * @return the access token
     * @throws NoSuchAlgorithmException the no such algorithm exception
     * @throws InvalidKeyException the invalid key exception
     * @throws SignatureException the signature exception
     * @throws JWTException the JWT exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static AccessTokenDTO getAccessToken(Client client, String authHost, String keyId, String privateKey) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, JWTException, IOException {
        /**
         * Nachdem die Registrierung erfolgreich abgelaufen ist, können wir Code zum signieren anfordern,
         * um die gültige Signature gegen ein AccessToken auszutauschen. Dazu benötigen wir die ID
         * unseres Schlüssels.
         */

        KeyIdentificationDTO identification = new KeyIdentificationDTO();

        if(keyId == null || "".equalsIgnoreCase(keyId))
            return null;

        identification.setKeyId(Integer.parseInt(keyId));
        ClientResponse response = client.resource(authHost + "/oauth2/signRequest").accept("application/json").type("application/json").post(ClientResponse.class, identification);
        if(response.getStatus() != 200) {
            logger.error("Auth.getAccessToken returned "+response.getStatus()+" on signRequest bailing out!");
            return null;
        }
        SignRequestDTO signRequest = response.getEntity(SignRequestDTO.class);

        /**
         * Den Code signieren und in Base64 encodieren.
         */
        Signature sig = Signature.getInstance(signRequest.getAlgorithm());
        sig.initSign(KeyLoader.loadPrivateKey(privateKey));
        sig.update(signRequest.getCode().getBytes());
        String signature = Base64.encodeBase64String(sig.sign());

        AccessTokenRequestDTO accessRequest = new AccessTokenRequestDTO();
        accessRequest.setCode(signRequest.getCode());
        accessRequest.setSignature(signature);

        response = client.resource(authHost + "/oauth2/access_token").accept("application/json").type("application/json").post(ClientResponse.class, accessRequest);

        if(response.getStatus() != 200) {
            logger.error("Auth.getAccessToken returned "+response.getStatus()+" bailing out!");
            return null;
        }

        AccessTokenDTO accessToken = response.getEntity(AccessTokenDTO.class);
        logger.trace("Access token: " + accessToken.getAccessToken());

/*
        // Prüfung des AccessToken
        // Lade Pubkey vom Auth
        logger.debug("Checke mit serverpubkey = "+Utils.getAB().getConfig().getString(Vocabulary.Config.Auth.ServerPubKey));
        PublicKey authPubKey = KeyLoader.loadKey(Utils.getAB().getConfig().getString(Vocabulary.Config.Auth.ServerPubKey));

        Date date = new Date();
        logger.debug("TS = " + date.getTime());

        // und testen
        JWTAccessToken token = new JWTAccessToken(authPubKey, accessToken.getAccessToken());
        if(!token.isValid()) {
            logger.error("Token ungültig");
        } else {
            logger.debug("AccessToken: "+accessToken.getAccessToken()+" , HEADER: "+accessToken.getHeader()+" , idToken:"+accessToken.getIdToken());
        }
*/
        return accessToken;
    }

    /**
     * Get the {@link JWTAccessToken} from the authorisation header.
     *
     * @param authorizationHeader the authorisation header value (e.g. "Bearer
     * eyJhbGciOiJSUzUxMiJ(...)4R5fe3RTMYFuBuId8uzz6aswpI"
     * @return the access token from the authorisation header, or null if there
     * is a issue with the authorisation header
     */
    public static JWTAccessToken getJwtAccessToken(final String authorizationHeader) {
        JWTAccessToken jwtAccesstoken = null;

        if (authorizationHeader != null) {
            String accessToken = authorizationHeader.replace("Bearer ", "");
            try {
                jwtAccesstoken = new JWTAccessToken(OAuthConfig.getOAuth2Client(ProjectInfo.INSTANCE.getProjectName()), accessToken);
            } catch (JWTException e) {
                logger.debug("Problems reading the access token: " + e);
            }
        }

        return jwtAccesstoken;
    }

    /**
     * Get the user authorisation ID from the authorisation header.
     *
     * @param authorizationHeader the authorisation header value (e.g. "Bearer
     * eyJhbGciOiJSUzUxMiJ(...)4R5fe3RTMYFuBuId8uzz6aswpI"
     * @return the user authorisation ID
     */
    public static String getUserAuthId(final String authorizationHeader) {
        String userAuthId = "";

        logger.debug("Getting user auth ID... ");

        JWTAccessToken jwtAccesstoken = getJwtAccessToken(authorizationHeader);
        if (jwtAccesstoken != null && jwtAccesstoken.isValid()) {
            userAuthId = jwtAccesstoken.getSubject();
        }

        logger.debug("User auth ID: " + userAuthId);

        return userAuthId;
    }
}
