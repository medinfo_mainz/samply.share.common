package de.samply.share.common.utils.oauth2;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import de.samply.auth.rest.Scope;
import de.samply.auth.utils.OAuth2ClientConfig;
import de.samply.common.config.OAuth2Client;
import de.samply.common.config.ObjectFactory;
import de.samply.config.util.JAXBUtil;
import de.samply.share.common.utils.ProjectInfo;

public class OAuthConfig {

    private final static String configFilename = "OAuth2Client.xml";

    private final static String DKFZ_LOGOUT_URI = "https://adfs1.dkfz.de/adfs/ls/?wa=wsignout1.0";

    private final static Logger logger = Logger.getLogger(OAuthConfig.class);

    /**
     * The xhtml loaded on OAuth server redirect.
     */
    protected static final String SAMPLY_OAUTH_RESPONSE_XHTML = "/samplyLogin.xhtml";

    /**
     * The xhtml loaded on OAuth server redirect.
     */
    protected static final String LOCAL_LOGOUT_REDIRECT_XHTML = "/loginRedirect.xhtml";

    /**
     * Get the url to be called for user authentication - from the authentication server. E.g. URL called when the user clicks on a "Login with Samply account" button.
     *
     * @return the URL to be called for user authentication
     * @throws UnsupportedEncodingException
     */
    public static String getAuthLoginUrl() throws UnsupportedEncodingException {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();

        String requestedPage = context.getRequestServletPath();
        if (requestedPage != null && requestedPage.startsWith("/") && requestedPage.length() > 1) {
            requestedPage = requestedPage.substring(1);
            if (requestedPage.endsWith(".xhtml") && requestedPage.length() > 6) {
                requestedPage = requestedPage.substring(0, requestedPage.length() - 6);
            }
        }
        Map<String, String> requestParameterMap = context.getRequestParameterMap();
        String inquiryId = requestParameterMap.get("inquiryId");

        StringBuilder sb = new StringBuilder();
        if (requestedPage != null && requestedPage.length() > 0) {
            sb.append("?requestedPage=");
            sb.append(requestedPage);
            if (inquiryId != null) {
                sb.append("&inquiryId=");
                sb.append(inquiryId);
            }
        }

        if (ProjectInfo.INSTANCE.getProjectName().equalsIgnoreCase("osse")) {
            return OAuth2ClientConfig.getRedirectUrl(getOAuth2Client("osse"), context.getRequestScheme(), context.getRequestServerName(), context.getRequestServerPort(),
                    context.getRequestContextPath(), SAMPLY_OAUTH_RESPONSE_XHTML + sb.toString(), Scope.OPENID);
        } else {
            return OAuth2ClientConfig.getRedirectUrl(getOAuth2Client(), context.getRequestScheme(), context.getRequestServerName(), context.getRequestServerPort(),
                    context.getRequestContextPath(), SAMPLY_OAUTH_RESPONSE_XHTML + sb.toString(), "DKFZ", Scope.OPENID);
        }
    }

    /**
     * Get the url to be called for user authentication - from the authentication server. E.g. URL called when the user clicks on a "logout" button.
     *
     * @return the URL to be called for user log out in the authentication server
     * @throws UnsupportedEncodingException
     */
    public static String getAuthLogoutUrl() throws UnsupportedEncodingException {
        String projectName = ProjectInfo.INSTANCE.getProjectName();
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();

        if (projectName.equalsIgnoreCase("dktk")) {
            String logoutUrl = getLogoutUrlDkfz(getOAuth2Client(), context.getRequestScheme(), context.getRequestServerName(), context.getRequestServerPort(),
                    context.getRequestContextPath(), LOCAL_LOGOUT_REDIRECT_XHTML);

            logger.debug("LogoutURL: " + logoutUrl);
            return logoutUrl;
        } else {
            return OAuth2ClientConfig.getLogoutUrl(getOAuth2Client(projectName), context.getRequestScheme(), context.getRequestServerName(),
                    context.getRequestServerPort(), context.getRequestContextPath(), LOCAL_LOGOUT_REDIRECT_XHTML);
        }
    }

    /**
     * Get the {@link OAuth2Client} instance, initialized with the value read from the {@link OsseFormEditorProperties}.
     *
     * @return
     */
    public static OAuth2Client getOAuth2Client() {
        return getOAuth2Client("dktk");
    }

    public static OAuth2Client getOAuth2Client(String projectContext) {
        OAuth2Client oauthClient = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
            oauthClient = JAXBUtil.findUnmarshall(configFilename, jaxbContext, OAuth2Client.class, projectContext, null);
        } catch (FileNotFoundException e) {
            logger.error("Could not find config file: " + configFilename + " for context " + projectContext);
        } catch (JAXBException e) {
            logger.error("Could not read config file");
        } catch (SAXException e) {
            logger.error("Could not read config file");
        } catch (ParserConfigurationException e) {
            logger.error("Could not read config file");
        }
        return oauthClient;
    }

    public static String getLogoutUrlDkfz(OAuth2Client config, String scheme,
            String serverName, int port, String contextPath, String localRedirectURL) throws UnsupportedEncodingException {

        String redirect = OAuth2ClientConfig.getLocalRedirectUrl(config, scheme,
                serverName, port,
                contextPath, localRedirectURL);

        String host = OAuth2ClientConfig.getHost(config, serverName);

        StringBuilder builder = new StringBuilder(host);
        builder.append("/logout.xhtml?redirect_uri=")
            .append(URLEncoder.encode(DKFZ_LOGOUT_URI, StandardCharsets.UTF_8.displayName()))
//            .append("&wreply=")
//            .append(URLEncoder.encode(redirect, StandardCharsets.UTF_8.displayName()))
            .append("&client_id=").append(URLEncoder.encode(config.getClientId(), StandardCharsets.UTF_8.displayName()));
        return builder.toString();
    }

}
