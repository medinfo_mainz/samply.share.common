package de.samply.share.common.control.uiquerybuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.context.FacesContext;
import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;
import org.omnifaces.model.tree.ListTreeModel;
import org.omnifaces.model.tree.TreeModel;

import de.samply.common.mdrclient.MdrClient;
import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.common.mdrclient.MdrInvalidResponseException;
import de.samply.common.mdrclient.domain.Validations;
import de.samply.share.common.model.uiquerybuilder.QueryItem;
import de.samply.share.common.model.uiquerybuilder.EnumConjunction;
import de.samply.share.common.model.uiquerybuilder.EnumOperator;
import de.samply.share.common.model.uiquerybuilder.QueryConverter;
import de.samply.share.common.utils.MdrDatatype;
import de.samply.share.model.osse.Query;
import de.samply.web.mdrFaces.MdrContext;
import javax.annotation.PostConstruct;

public abstract class AbstractSearchController implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 585856018106743936L;

    /** The logger. */
    private transient Logger logger = Logger.getLogger(this.getClass().getName());

    /** The mdr client. */
    private transient MdrClient mdrClient = MdrContext.getMdrContext().getMdrClient();

    /** The sortable item panel. */
    private transient HtmlPanelGroup sortableItemPanel;
    
    /** The Constant CONJUNCTION_GROUP. */
    private static final String CONJUNCTION_GROUP_ID = "conjunctionGroup";
    
    /** The query tree. */
    protected TreeModel<QueryItem> queryTree;

    /** The list of available conjunctions. */
    private static List<String> conjunctionList;
    static {
        conjunctionList = new ArrayList<String>();
        conjunctionList.add(EnumConjunction.AND.toString());
        conjunctionList.add(EnumConjunction.OR.toString());
    }
    
    /**
     * Restores query tree if the serialized query is not empty
     */
    @PostConstruct
    public void init() {
        String serializedQuery = getSerializedQuery();
        if (getSerializedQuery() != null && !serializedQuery.isEmpty()) {
            restoreQuery();
        }
    }

    /**
     * Gets the conjunction list.
     *
     * @return the conjunction list
     */
    public List<String> getConjunctionList() {
        return conjunctionList;
    }
  
    /**
     * Must be overridden to provide the serialized query as string.
     * @return the Query, serialized as XML string
     */
    public abstract String getSerializedQuery();

    /**
     * Must be overridden to set the query as string.
     * @param serializedQuery the Query, serialized as XML string
     */
    public abstract void setSerializedQuery(String serializedQuery);

    /**
     * Gets the query tree.
     *
     * @return the query tree
     */
    public TreeModel<QueryItem> getQueryTree() {
        return queryTree;
    }

    /**
     * Sets the query tree.
     *
     * @param queryTree
     *            the new query tree
     */
    public void setQueryTree(TreeModel<QueryItem> queryTree) {
        this.queryTree = queryTree;
    }

    /**
     * Adds the current mdr item to query root. The mdr id of said element is stored in the request parameter map during the drop event from jquery ui.
     */
    public void addItemToQueryRoot() {
        String mdrId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("mdrId");

        TreeModel<QueryItem> node = queryTree;

        QueryItem queryItem = new QueryItem();
        queryItem.setMdrId(mdrId);
        node.addChild(queryItem);
    }

    /**
     * Adds the current mdr item to the conjunction group it was dropped on. The mdr id of said element as well as the group id are stored in the request parameter map during the drop event from jquery ui.
     */
    public void addItemToQueryGroup() {
        String mdrId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("mdrId");
        String tmpId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("tmpId");

        TreeModel<QueryItem> node = null;

        node = getNodeByTmpId(queryTree, tmpId);

        if (node == null) {
            logger.debug("No matching id found " + tmpId + " ...adding to root");
            node = queryTree;
        }

        // go back through tree to check for conj group or use root
        node = getParentConjunctionGroup(node);

        QueryItem queryItem = new QueryItem();
        queryItem.setMdrId(mdrId);
        node.addChild(queryItem);
    }

    /**
     * Gets the treenode in the queryTree for the given tmp id.
     *
     * @param node
     *            the (relative) rootnode from wher the search is started
     * @param tmpId
     *            the tmp id of the element to be looked for
     * @return the matching node
     */
    private TreeModel<QueryItem> getNodeByTmpId(TreeModel<QueryItem> node, String tmpId) {

        if (!node.isRoot() && node.getData().getTempId().equalsIgnoreCase(tmpId)) {
            return node;
        }

        TreeModel<QueryItem> foundNode = null;
        List<TreeModel<QueryItem>> childNodes = node.getChildren();

        for (int i = 0; foundNode == null && i < childNodes.size(); ++i) {
            foundNode = getNodeByTmpId(childNodes.get(i), tmpId);
        }

        return foundNode;
    }

    /**
     * Gets the parent conjunction group for a given treenode by traversing the tree in bottom-up direction.
     *
     * @param node
     *            the node to check
     * @return the first ancestor that is a conjunction group
     */
    private TreeModel<QueryItem> getParentConjunctionGroup(TreeModel<QueryItem> node) {
        if (node.isRoot()) {
            return node;
        }

        if (node.getData().getMdrId() != null && node.getData().getMdrId().equalsIgnoreCase(CONJUNCTION_GROUP_ID)) {
            return node;
        }
        return getParentConjunctionGroup(node.getParent());
    }

    /**
     * Adds a child node to the query tree.
     *
     * @param node
     *            the node to which the new node will be attached
     * @param mdrId
     *            the mdr id of the new node
     */
    public void addChild(TreeModel<QueryItem> node, String mdrId) {
        QueryItem queryItem = new QueryItem();
        queryItem.setMdrId(mdrId);
        node.addChild(queryItem);
    }

    /**
     * Clones the node from the query tree as a sibling.
     *
     * @param node
     *            the node
     */
    public void clone(TreeModel<QueryItem> node) {
        QueryItem original = node.getData();
        QueryItem clone = new QueryItem();
        clone.setMdrId(original.getMdrId());
        clone.setOperator(original.getOperator());
        node.getParent().addChild(clone);
    }

    /**
     * Removes the node from the query tree.
     *
     * @param node
     *            the node
     */
    public void remove(TreeModel<QueryItem> node) {
        node.remove();
    }

    /**
     * Instantiates a new search controller.
     * Adds the initial conjunction group.
     */
    public AbstractSearchController() {
        queryTree = new ListTreeModel<>();

        QueryItem rootGroup = new QueryItem();
        rootGroup.setConjunction(EnumConjunction.AND);
        rootGroup.setMdrId(CONJUNCTION_GROUP_ID);
        rootGroup.setRoot(true);

        queryTree.addChild(rootGroup);

    }

    /**
     * On submit.
     *
     * @return the string
     */
    public abstract String onSubmit();

    /**
     * Gets the sortable item panel.
     *
     * @return the sortable item panel
     */
    public HtmlPanelGroup getSortableItemPanel() {
        return sortableItemPanel;
    }

    /**
     * Sets the sortable item panel.
     *
     * @param sortableItemPanel
     *            the new sortable item panel
     */
    public void setSortableItemPanel(HtmlPanelGroup sortableItemPanel) {
        this.sortableItemPanel = sortableItemPanel;
    }

    /** The operator list. */
    private static final List<String> operatorList;
    static {
        operatorList = new ArrayList<>();
        operatorList.add(EnumOperator.EQUAL.toString());
        operatorList.add(EnumOperator.GREATER.toString());
        operatorList.add(EnumOperator.GREATER_OR_EQUAL.toString());
        operatorList.add(EnumOperator.IS_NOT_NULL.toString());
        operatorList.add(EnumOperator.IS_NULL.toString());
        operatorList.add(EnumOperator.LESS_OR_EQUAL_THEN.toString());
        operatorList.add(EnumOperator.LESS_THEN.toString());
        operatorList.add(EnumOperator.LIKE.toString());
        operatorList.add(EnumOperator.NOT_EQUAL_TO.toString());
        operatorList.add(EnumOperator.BETWEEN.toString());
        // operatorList.add(EnumOperator.IN.toString());
    }

    /**
     * Gets the operator list.
     *
     * @return the operator list
     */
    public List<String> getOperatorList() {
        return operatorList;
    }

    /**
     * Generate the operator list based on the validation type of the given mdrkey.
     *
     * @param mdrId
     *            the mdr id
     * @return the list of suitable operators for that element
     * @throws MdrConnectionException
     *             the mdr connection exception
     * @throws MdrInvalidResponseException
     *             the mdr invalid response exception
     * @throws ExecutionException
     *             the execution exception
     */
    public List<String> getOperatorList(String mdrId) throws MdrConnectionException, MdrInvalidResponseException, ExecutionException {
        Validations validations = mdrClient.getDataElementValidations(mdrId, "en");
        List<String> ops = new ArrayList<>();
        MdrDatatype dataType = MdrDatatype.get(validations.getDatatype());
        
        ops.add(EnumOperator.EQUAL.toString());
        ops.add(EnumOperator.NOT_EQUAL_TO.toString());

        switch(dataType) {
            case FLOAT:
            case INTEGER:
            case DATE:
            case TIME:
            case DATETIME:
                ops.add(EnumOperator.GREATER.toString());
                ops.add(EnumOperator.GREATER_OR_EQUAL.toString());
                ops.add(EnumOperator.LESS_OR_EQUAL_THEN.toString());
                ops.add(EnumOperator.LESS_THEN.toString());
                ops.add(EnumOperator.BETWEEN.toString());
                break;
            case STRING:
                ops.add(EnumOperator.LIKE.toString());
                break;
        }
        
        // TODO: EnumOperator.IN

        ops.add(EnumOperator.IS_NOT_NULL.toString());
        ops.add(EnumOperator.IS_NULL.toString());

        return ops;
    }

    /**
     * @return true if the query tree contains an actual query; false otherwise
     */
    public boolean isQueryTreeNonTrivial() {
        return queryTree.getChildCount() > 0 
                && queryTree.getChildren().get(0) != null 
                && queryTree.getChildren().get(0).getChildCount() > 0;
    }
    
    /**
     * Performs serialization of the Query.
     */
    public void serializeQuery() {
        Query query;
        if (isQueryTreeNonTrivial()) {
            query = QueryConverter.treeToQuery(queryTree);
        }
        else {
            query = new Query(); 
        }
        
        String queryAsXml = "";
        try {
            queryAsXml = QueryConverter.queryToXml(query);
        } catch (JAXBException e) {
            logger.error("JAXB Exception while trying to convert query to xml.");
            return;
        }
        setSerializedQuery(queryAsXml);
    }

    /**
     * Restores the Query using the serialized string.
     */
    public void restoreQuery() {
        String serializedQuery = getSerializedQuery();
        if (serializedQuery != null && serializedQuery.length() > 0 && !serializedQuery.equalsIgnoreCase("null") ) {
            logger.debug("restoring query and refreshing page. Serialized query is " + serializedQuery);
            queryTree = QueryConverter.queryStringToTree(serializedQuery);
            if (queryTree.getChildCount() == 0) {
                logger.debug("Query Tree was empty. Adding Default Group.");
                QueryItem queryItem = new QueryItem();
                queryItem.setMdrId(CONJUNCTION_GROUP_ID);
                queryItem.setRoot(true);
                queryTree.addChild(queryItem);
            }
        }
    }

}
