if (typeof(restorationProperties) == "undefined") {
    new Error("Restoration properties need to be defined for query restoration");
}

function writeValuesToLocalStorage() {
    for (var localStorageKey in restorationProperties) {
        // get value that should be saved from view
        var valueToSave = $(restorationProperties[localStorageKey]).val();
        if (valueToSave === "" || valueToSave === null) {
            localStorage.removeItem(localStorageKey); // don't save empty strings or nulls
        }
        else { // save value in local storage
            localStorage.setItem(localStorageKey, valueToSave);
        }
    }
}

function moveValuesFromLocalStorageToDom() {
    for (var localStorageKey in restorationProperties) {
        $(restorationProperties[localStorageKey]).val(localStorage.getItem(localStorageKey));
    }
}

function deleteValuesFromLocalStorage() {
    for (var localStorageKey in restorationProperties) {
        localStorage.removeItem(localStorageKey);
    }
}

function valuesInLocalStorage() {
    for (var localStorageKey in restorationProperties) {
        var localStorageItem = localStorage.getItem(localStorageKey);
        if (localStorageItem != null && localStorageItem.length > 0) {
            return true;
        }
    }
    return false;
}

function dismissValues() {
    $('#pleaseWait').modal('show');
    deleteValuesFromLocalStorage();
    location.reload();
}